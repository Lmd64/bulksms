//
//  MessagesStackViewController.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit

public protocol MessagesStackDelegate {
    func send()
    func cancel()
}

public class MessagesStackViewController :UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var messagesStack: MessagesStack!
    @IBOutlet weak var sendToContactsLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var messagesStackDelegate: MessagesStackDelegate?
    var blurEffectView :UIVisualEffectView? = nil

    var message: Message? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let message = self.message {
        
            if blurEffectView == nil {
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                self.blurEffectView = blurEffectView
                blurEffectView.translatesAutoresizingMaskIntoConstraints = false
                view.insertSubview(blurEffectView, belowSubview: backgroundView)
                blurEffectView.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor).active = true
                blurEffectView.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor).active = true
                blurEffectView.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor).active = true
                blurEffectView.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor).active = true
            }
            
            backgroundView.layer.cornerRadius = 8.0
            
            if let messagesStack = self.messagesStack {
                if let contact = message.contact?.allObjects.first as? Contact {
                    messagesStack.message = message.messageForContact(contact)
                }
            }
            
            if let sendToContactsLabel = self.sendToContactsLabel {
                if let count = message.contact?.count {
                    sendToContactsLabel.text = "Send to \(count) contacts"
                }
            }
            
        }
    }
    
    @IBAction func didTapSendButton(sender: AnyObject) {
        messagesStackDelegate?.send()
    }
    
    @IBAction func didTapCancelButton(sender: AnyObject) {
        messagesStackDelegate?.cancel()
    }

}
