//
//  MessageViewController.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class MessageViewController: UIViewController, UITextFieldDelegate, MFMessageComposeViewControllerDelegate, MessagesStackDelegate {

    var managedObjectContext: NSManagedObjectContext? = nil
    
    @IBOutlet weak var senderNameTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    var currentContact :Contact? = nil
    var messagesStackViewController :MessagesStackViewController? = nil

    override func viewDidAppear(animated: Bool) {
        configureView()
    }
    
    var message: Message? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let message = self.message {

            if let senderNameTextField = self.senderNameTextField {
                senderNameTextField.text = message.senderName
            }

            if let messageTextView = self.messageTextView {
                messageTextView.text = message.text
                messageTextView.layer.borderColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5).CGColor
                messageTextView.layer.borderWidth = 0.5
                messageTextView.layer.cornerRadius = 4.0
            }
            
            if let sendButton = self.sendButton {
                sendButton.enabled = false
                sendButton.setTitle("No contacts added yet", forState: .Normal)
                if let count = message.contact?.count {
                    if count > 0 {
                        sendButton.enabled = true
                        sendButton.setTitle("Send to \(count) contacts", forState: .Normal)
                    }
                }
            }
            
        }
    }

    override func viewWillDisappear(animated: Bool) {
        saveMessage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addContactsSegue" {
            let controller = (segue.destinationViewController) as! ContactsTableViewController
            controller.message = message
            controller.managedObjectContext = managedObjectContext
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    func sortedContacts() -> [Contact] {
        let sortDescriptor = NSSortDescriptor(key: "phoneNumber", ascending: true)
        let sortedContacts = message?.contact?.sortedArrayUsingDescriptors([sortDescriptor]) as! [Contact]
        //let sortedContacts = message?.contact?.allObjects
        return sortedContacts
    }
    
    @IBAction func didTapSendButton(sender: AnyObject) {
        saveMessage()
        currentContact = (sortedContacts() as NSArray).firstObject as? Contact
        if (currentContact != nil) {
            presentMessageStackPreview()
        }
    }
    
    func presentMessageStackPreview(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewControllerWithIdentifier("MessagesStackViewController") as? MessagesStackViewController {
            self.messagesStackViewController = controller
            controller.messagesStackDelegate = self
            controller.message = message
            controller.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(controller.view)
            controller.view.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor).active = true
            controller.view.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor).active = true
            controller.view.bottomAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor).active = true
            controller.view.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor).active = true
            
            var transform = CGAffineTransformIdentity
            transform = CGAffineTransformTranslate(transform, 0, CGRectGetHeight(view.frame))
            transform = CGAffineTransformScale(transform,0.05, 0.05)
            
            controller.view.alpha = 0.0
            controller.backgroundView.transform = transform
            
            UIView.animateWithDuration(0.32, animations: {
                controller.view.alpha = 1.0
                controller.backgroundView.transform = CGAffineTransformIdentity
                }, completion: nil)

        }
    }

    func dismissMessageStackPreview(){
        if let controller = self.messagesStackViewController {

            var transform = CGAffineTransformIdentity
            transform = CGAffineTransformTranslate(transform, 0, CGRectGetHeight(view.frame))
            transform = CGAffineTransformScale(transform,0.05, 0.05)

            controller.view.alpha = 1.0
            controller.backgroundView.transform = CGAffineTransformIdentity
            
            UIView.animateWithDuration(0.24, animations: {
                controller.view.alpha = 0.0
                controller.backgroundView.transform = transform
                }, completion: {
                    (finished :Bool) in
                    self.messagesStackViewController?.view.removeFromSuperview()
            })
        }
    }

    func send(){
        sendMessageToContact(currentContact!)
    }
    
    func cancel(){
        dismissMessageStackPreview()
    }
    
    func sendMessageToContact(contact: Contact) {
        if contact.messageComposeResult != 3 {
            let messageVC = MFMessageComposeViewController()
            messageVC.body = message!.messageForContact(contact)
            messageVC.recipients = [contact.phoneNumber!]
            messageVC.messageComposeDelegate = self;
            self.presentViewController(messageVC, animated: false, completion: nil)
        } else {
            sendMessageToNextContact()
        }
    }
    
    func sendMessageToNextContact(){
        if currentContact != (sortedContacts() as NSArray).lastObject as? Contact {
            let sortedContactsArray = sortedContacts()
            let currentContactIndex = (sortedContactsArray as NSArray).indexOfObject(currentContact!)
            currentContact = sortedContactsArray[currentContactIndex+1]
            sendMessageToContact(currentContact!)
        }
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult){

        switch (result) {
        case MessageComposeResultCancelled:
            print("Message was cancelled")
            currentContact?.messageComposeResult = 1
            self.dismissViewControllerAnimated(true, completion: {
                self.sendMessageToNextContact()
            })

        case MessageComposeResultFailed:
            print("Message failed")
            currentContact?.messageComposeResult = 2
            self.dismissViewControllerAnimated(true, completion: {
                self.sendMessageToNextContact()
            })

        case MessageComposeResultSent:
            print("Message was sent")
            currentContact?.messageComposeResult = 3
            self.dismissViewControllerAnimated(true, completion: {
                self.sendMessageToNextContact()
            })
        default:
            break;
        }

        do {
            try managedObjectContext?.save()
        } catch {
            print("Unresolved error \(error)")
            abort()
        }

    }

    func saveMessage(){
        if let messageTextView = self.messageTextView {
            message?.text = messageTextView.text
        }
        if let senderNameTextField = self.senderNameTextField {
            message?.senderName = senderNameTextField.text
        }
        do {
            try managedObjectContext?.save()
        } catch {
            print("Unresolved error \(error)")
            abort()
        }
    }
    
}

