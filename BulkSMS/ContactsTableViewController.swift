//
//  ContactsTableViewController.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class ContactsTableViewController: UITableViewController {

    var managedObjectContext: NSManagedObjectContext? = nil
    var message: Message? = nil

    var actionBarButton: UIBarButtonItem!
    var contacts :[Contact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.reloadData()
        
        reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: //not yet sent
            return contactsNotYetSent().count
            break
            
        case 1: //cancelled
            return contactsSendCancelled().count
            break
            
        case 2: //failed
            return contactsSendFailed().count
            break
            
        case 3: //sent
            return contactsSendSucceeded().count
            break
            
        default:
            return 0
            break
        }
        //return contacts.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("contactCellIdentifier", forIndexPath: indexPath)
        
        var sectionContacts :[Contact] = []
        switch indexPath.section {
        case 0: //not yet sent
            sectionContacts = contactsNotYetSent()
            break
            
        case 1: //cancelled
            sectionContacts = contactsSendCancelled()
            break
            
        case 2: //failed
            sectionContacts = contactsSendFailed()
            break
            
        case 3: //sent
            sectionContacts = contactsSendSucceeded()
            break
            
        default:
            sectionContacts = []
            break
        }
        
        let contact = sectionContacts[indexPath.row]
        cell.textLabel?.text = contact.name
        cell.detailTextLabel?.text = contact.phoneNumber

        cell.textLabel?.backgroundColor = UIColor.clearColor()
        cell.detailTextLabel?.backgroundColor = UIColor.clearColor()

        cell.textLabel?.textColor = UIColor.darkGrayColor()
        cell.detailTextLabel?.textColor = UIColor.darkGrayColor()

        cell.accessoryType = UITableViewCellAccessoryType.None
        
        switch indexPath.section {
        case 0: //not yet sent
            cell.textLabel?.textColor = UIColor.blackColor()
            cell.detailTextLabel?.textColor = UIColor.blackColor()
            cell.backgroundColor = UIColor.whiteColor()

        case 1: //cancelled
            cell.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.2)
            
        case 2: //failed
            cell.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.2)
            
        case 3: //sent
            cell.backgroundColor = UIColor.greenColor().colorWithAlphaComponent(0.2)
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            
        default: //anything else
            cell.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        }
    
        cell.selectionStyle = .None
        
        return cell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: //not yet sent
            return "Pending (\(contactsNotYetSent().count) contacts)"
            
        case 1: //cancelled
            return "Cancelled (\(contactsSendCancelled().count) contacts)"
            
        case 2: //failed
            return "Failed (\(contactsSendFailed().count) contacts)"
            
        case 3: //sent
            return "Sent (\(contactsSendSucceeded().count) contacts)"
            
        default: //anything else
            return "Anything Else"
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            let contact = contacts[indexPath.row]
            managedObjectContext?.deleteObject(contact)
            contacts.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    func reloadData() {
        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let phonenumberSortDescriptor = NSSortDescriptor(key: "phoneNumber", ascending: true)
        contacts = message?.contact?.sortedArrayUsingDescriptors([nameSortDescriptor,phonenumberSortDescriptor]) as! [Contact]

        actionBarButton = UIBarButtonItem(title: "Import", style: .Plain, target: self, action: #selector(didTapActionBarButton(_:)))
        self.navigationItem.setRightBarButtonItem(actionBarButton, animated: true)
        actionBarButton.enabled = true
        tableView.reloadData()
    }
    
    @IBAction func didTapActionBarButton(sender: AnyObject) {
        parseClipboard()
        reloadData()
    }

    func parseClipboard(){
        let clipboard = UIPasteboard.generalPasteboard()
        if let clipboardContents = clipboard.valueForPasteboardType("public.plain-text" as String) as? String {
            //print("\(clipboardContents)")
            let clipboardContacts = clipboardContents.componentsSeparatedByString("\n")
            for contactString in clipboardContacts {
                if let contactDetails :[String] = contactString.componentsSeparatedByString(",") {
                    if contactDetails.count == 2 {
                        let name = contactDetails[0]
                        let phoneNumber = contactDetails[1]
                        insertNewContact(name, phoneNumber: phoneNumber)
                    }
                }
            }
        }
        reloadData()
        
    }
    
    func insertNewContact(name: String, phoneNumber: String) {
        print("\(name) - > \(phoneNumber)")
        if let context = self.managedObjectContext {
            
            if let contact = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: context) as? Contact {
                
                // If appropriate, configure the new managed object.
                // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
                contact.name = name
                contact.phoneNumber = phoneNumber
                contact.message = message
                
                // Save the context.
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    //print("Unresolved error \(error), \(error.userInfo)")
                    abort()
                }
            }
        }
        
    }

    func contactsNotYetSent() -> [Contact] {
        return contacts.filter() { $0.messageComposeResult == 0 }
    }
    func contactsSendCancelled() -> [Contact] {
        return contacts.filter() { $0.messageComposeResult == 1 }
    }
    func contactsSendFailed() -> [Contact] {
        return contacts.filter() { $0.messageComposeResult == 2 }
    }
    func contactsSendSucceeded() -> [Contact] {
        return contacts.filter() { $0.messageComposeResult == 3 }
    }
    
}
