//
//  Message.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import Foundation
import CoreData


class Message: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    func messageForContact(contact :Contact) -> String {
        if let _parsedMessage = text {
            var parsedMessage :String = _parsedMessage
            parsedMessage = parsedMessage.stringByReplacingOccurrencesOfString("<contact.name>", withString:contact.name!)
            if let senderName = senderName {
                parsedMessage = parsedMessage.stringByReplacingOccurrencesOfString("<sender.name>", withString:senderName)
            } else {
                parsedMessage = parsedMessage.stringByReplacingOccurrencesOfString("<sender.name>", withString:"")
            }
            parsedMessage = parsedMessage.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            return parsedMessage
        } else {
            return ""
        }
    }

}
