//
//  Message+CoreDataProperties.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Message {

    @NSManaged var text: String?
    @NSManaged var timestamp: NSDate?
    @NSManaged var senderName: String?
    @NSManaged var contact: NSSet?

}
