//
//  CustomViews.swift
//  BulkSMS
//
//  Created by Liam Dunne on 05/04/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit

@IBDesignable
class MessagesStack: UIView {
    
    @IBInspectable var message = ""
    
    override func drawRect(rect :CGRect){
        StyleKit.drawMessagesStack(frame: rect, message: message)
    }
    // draw the 'email' icon by default in Interface Builder
    override func prepareForInterfaceBuilder() {
        message = "Test Message"
    }
}